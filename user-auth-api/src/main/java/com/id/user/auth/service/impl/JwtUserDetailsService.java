package com.id.user.auth.service.impl;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.stereotype.Service;

import com.id.user.auth.repository.UserRepository;

@Service
public class JwtUserDetailsService implements UserDetailsService {
	
	@Autowired
	UserRepository userRepository;
	
	public com.id.user.auth.model.User loadUserByEmailOrHp(String emailHp){
		
		com.id.user.auth.model.User user = userRepository.findByEmailOrHandphone(emailHp,emailHp);
		
		return user;
	}

	@Override
	public UserDetails loadUserByUsername(String emailHp) throws UsernameNotFoundException {
		
		com.id.user.auth.model.User user = userRepository.findByEmailOrHandphone(emailHp,emailHp);
		
		if(user != null) {
			return new User(user.getEmail(), user.getPassword(), new ArrayList<>());
		} else {
			throw new UsernameNotFoundException("User not found with Email / Handphone: " + emailHp);
		}
	}
	
	public UserDetails loadUserByUsernamePassword(String emailHp, String password) throws UsernameNotFoundException {
		
		com.id.user.auth.model.User user = userRepository.findByEmailOrHandphone(emailHp,emailHp);
		
		boolean passwordValid = false;
		if(user != null) {
			passwordValid = BCrypt.checkpw(password,user.getPassword());
		}
		
		if(passwordValid) {
			return new User(user.getEmail(), user.getPassword(), new ArrayList<>());
		} else {
			throw new UsernameNotFoundException("User not found with Email / Handphone: " + emailHp);
		}
		
	}
	
}
