package com.id.user.auth.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.id.user.auth.config.JwtTokenUtil;
import com.id.user.auth.model.JwtResponse;
import com.id.user.auth.model.User;
import com.id.user.auth.service.impl.JwtUserDetailsService;

import io.jsonwebtoken.ExpiredJwtException;

@RestController
@CrossOrigin
public class JwtAuthenticationController {

	@Autowired
	private AuthenticationManager authenticationManager;
	
	@Autowired
	private JwtTokenUtil jwtTokenUtil;
	
	@Autowired
	private JwtUserDetailsService jwtUserDetailsService;
	
	@RequestMapping(value = "/auth", method = RequestMethod.GET )
	public ResponseEntity<?> auth() throws Exception {
		return ResponseEntity.ok().build();
	}
	
	@RequestMapping(value = "/authenticate", method = RequestMethod.POST)
	public ResponseEntity<?> createAuthenticationToken(@RequestHeader HttpHeaders httpHeaders) throws Exception {
		User user = jwtTokenUtil.getUserAuthorization(httpHeaders.getFirst(HttpHeaders.AUTHORIZATION));
		
		if(user != null && !StringUtils.isEmpty(user.getHandphone())) {
			User userNew = jwtUserDetailsService.loadUserByEmailOrHp(user.getHandphone());
			user.setEmail(userNew.getEmail());
		}
		
		authenticate(user.getEmail(), user.getPassword());
		
		final UserDetails userDetails = jwtUserDetailsService.loadUserByUsernamePassword(user.getEmail(), user.getPassword());
		
		Map<String, Object> result = new HashMap<String, Object>();
		
		if(userDetails == null) {
			result.put("status", HttpStatus.UNAUTHORIZED.value());
			result.put("messages", "Unauthorized User");
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(result);
		}
		
		Map<String, Object> claims = new HashMap<String, Object>();
		claims.put("scope", "read write");
		final String token = jwtTokenUtil.doGenerateToken(claims, userDetails.getUsername());
		
		return ResponseEntity.ok(new JwtResponse(token));
	}
	
	@RequestMapping(value = "/getSubject", method = RequestMethod.GET)
	public ResponseEntity<?> getSubjectToken(@RequestHeader HttpHeaders httpHeaders) throws Exception {
		
		JwtResponse jwtToken = jwtTokenUtil.getTokenAuthorization(httpHeaders.getFirst(HttpHeaders.AUTHORIZATION));
		
		String subject = null;
		String message = "";
		
		// JWT Token is in the form "Bearer token". Remove Bearer word and get
		// only the Token
		if (jwtToken != null) {
			try {
				subject = jwtTokenUtil.getUsernameFromToken(jwtToken.getToken());
			} catch (IllegalArgumentException e) {
				message = "Unable to get JWT Token";
			} catch (ExpiredJwtException e) {
				message = "JWT Token has expired";
			}
		} else {
			message = "JWT Token does not begin with Bearer String";
		}
		
		Map<String, Object> result = new HashMap<String, Object>();
		
		if(subject == null) {
			result.put("status", HttpStatus.UNAUTHORIZED.value());
			result.put("messages", message);
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(result);
		}
		
		return ResponseEntity.ok(subject);
	}
	
	private void authenticate(String username, String password) throws Exception {
		try {
			authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
		} catch (DisabledException e) {
			throw new Exception("USER_DISABLED", e);
		} catch (BadCredentialsException e) {
			throw new Exception("INVALID_CREDENTIALS", e);
		}
	}
	
}
