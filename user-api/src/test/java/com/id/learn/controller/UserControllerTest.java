	package com.id.learn.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import com.github.javafaker.Faker;
import com.id.learn.AbstractTest;
import com.id.user.controller.AliveController;
import com.id.user.controller.UserController;
import com.id.user.model.db.dataSourceOne.User;
import com.id.user.service.UserService;

@DisplayName("Authentication Testing")
@ExtendWith(MockitoExtension.class)
@WebMvcTest(controllers = {UserController.class, AliveController.class})
public class UserControllerTest extends AbstractTest {
	
	Faker faker;
	
    @Autowired
    MockMvc mockMvc;
	
	@MockBean
	UserService userService;
	
	@Before
    private void before(){
		System.out.println("ABC");
		faker = new Faker();
    }

	@Test
	public void getUserList() throws Exception {
		
		String uri = "http://localhost:8083/myapps/user/getAll";
		MvcResult mvcResult = mockMvc.perform(get(uri)
				.contentType(MediaType.APPLICATION_JSON)
				.accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk()).andReturn();
	      
        //get responseBody
        String responseBody = mvcResult.getResponse().getContentAsString();
		
		List<User> users = new ArrayList<User>();
		User user = new User();
		user.setAddress(faker.address().fullAddress());
		user.setFirstname(faker.name().firstName());
		user.setLastname(faker.name().lastName());
		user.setPhone(faker.phoneNumber().phoneNumber());
		user.setUsername(faker.funnyName().name());
		
		System.out.println(">>>>>"+ user.toString());
		users.add(user);
		
		Mockito.when(userService.getAllUser()).thenReturn(users);
		
		Assert.assertTrue(users.size() > 0);
	}
	
}
