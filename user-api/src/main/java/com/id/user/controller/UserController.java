package com.id.user.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.id.user.config.ValidatorFactoryCustom;
import com.id.user.model.db.dataSourceOne.User;
import com.id.user.model.var.Pagearea;
import com.id.user.service.UserService;

@RestController
@RequestMapping(value = "/user")
public class UserController extends ValidatorFactoryCustom<User> {

	@Autowired
	private UserService userService;
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public ResponseEntity<?> update(@RequestBody User user, @RequestHeader HttpHeaders headers) throws Exception {
		
		Map<String, Object> map = userService.updateUser(user, headers);
		
		if(((Integer) map.get("status")) == HttpStatus.OK.value()) {
			return ResponseEntity.ok(map.get("message"));
		}
		
		return ResponseEntity.status(HttpStatus.valueOf((Integer) map.get("status"))).body(map);
	}
	
	@RequestMapping(value = "/getProfile", method = RequestMethod.GET)
	public ResponseEntity<?> getProfile(@RequestHeader HttpHeaders headers) throws Exception {
		
		return ResponseEntity.status(HttpStatus.OK).body(userService.getUserProfile(headers));
	}
	
	@RequestMapping(value = "/getAll", method = RequestMethod.GET)
	public ResponseEntity<?> getAll() throws Exception {
		
		return ResponseEntity.status(HttpStatus.OK).body(userService.getAllUser());
	}
	
	@RequestMapping(value = "/getPageable", method = RequestMethod.POST)
	public ResponseEntity<?> getPageable(Pagearea pagearea) throws Exception {
		
		return ResponseEntity.status(HttpStatus.OK).body(userService.getPageableUser(pagearea));
	}
	
}
