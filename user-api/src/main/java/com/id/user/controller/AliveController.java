package com.id.user.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.id.user.config.InternationalizationConfig;

@RestController
@EnableAutoConfiguration
public class AliveController {
	
	@Autowired
	MessageSource messageSource;

	@RequestMapping(value = "/alive", method = RequestMethod.GET)
	public String alive(@RequestHeader HttpHeaders headers) throws Exception {
		
		String[] params = new String[]{"still alive"};
		
		return messageSource.getMessage("title.application", params, InternationalizationConfig.getLocaleFromHeaders(headers));
	}
}
