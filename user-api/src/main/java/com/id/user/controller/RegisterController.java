package com.id.user.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.id.user.model.db.dataSourceOne.User;
import com.id.user.service.UserService;

@RestController
@RequestMapping(value = "/")
public class RegisterController {

	@Autowired
	private UserService userService;
	
	@RequestMapping(value = "/register", method = RequestMethod.POST)
	public ResponseEntity<?> register(@RequestBody User user, @RequestHeader HttpHeaders headers) throws Exception {
		
		user.setId(null);
		Map<String, Object> map = userService.registerUser(user, headers);
		
		if(((Integer) map.get("status")) == HttpStatus.OK.value()) {
			return ResponseEntity.ok(map.get("message"));
		}
		
		return ResponseEntity.status(HttpStatus.valueOf((Integer) map.get("status"))).body(map);
	}
	
}
