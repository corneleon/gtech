package com.id.user.config;

import java.util.Locale;

import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.HttpHeaders;

public class InternationalizationConfig {

	public static Locale getLocaleFromHeaders(HttpHeaders headers) {
		
		Locale locale = LocaleContextHolder.getLocale();
		if(headers.getAcceptLanguage() != null && headers.getAcceptLanguage().size() > 0) {
			locale = new Locale(headers.getAcceptLanguage().get(0).getRange());
		}
		
		return locale;
		
	}
	
}
