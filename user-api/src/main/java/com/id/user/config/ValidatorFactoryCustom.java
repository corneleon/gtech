package com.id.user.config;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpHeaders;

public class ValidatorFactoryCustom<T> {
	
	@Autowired
	MessageSource messageSource;

	public List<String> getErrorListFromModel(T t, HttpHeaders headers){
		ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
		Validator validator = factory.getValidator();
		Set<ConstraintViolation<T>> violations = validator.validate(t);
		
		List<String> listError = new ArrayList<String>();
		violations.forEach(violation -> {
			listError.add(messageSource.getMessage(violation.getMessage(), null, InternationalizationConfig.getLocaleFromHeaders(headers))); 
		});
		
		return listError;
	}
	
}
