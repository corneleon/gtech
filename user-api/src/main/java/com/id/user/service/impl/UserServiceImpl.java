package com.id.user.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Order;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.client.RestTemplate;

import com.id.user.config.ValidatorFactoryCustom;
import com.id.user.model.db.dataSourceOne.User;
import com.id.user.model.var.Pagearea;
import com.id.user.repository.dataSourceOne.UserRepository;
import com.id.user.service.UserService;

@Service
public class UserServiceImpl extends ValidatorFactoryCustom<User> implements UserService {
	
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private Environment env;
	
	@Override
	public Map<String,Object> registerUser(User user, HttpHeaders headers) throws Exception {
		Map<String, Object> result = new HashMap<String, Object>();
		
		List<String> errorList = getErrorListFromModel(user, headers);
		
		if(errorList != null && errorList.size() > 0) {
			result.put("status", HttpStatus.BAD_REQUEST.value());
			result.put("messages", errorList);
		} else {
			user.setPassword(BCrypt.hashpw(user.getPassword(), BCrypt.gensalt(10)));
			try {
				userRepository.save(user);
				result.put("status", HttpStatus.OK.value());
				result.put("messages", user);
			} catch (Exception e) {
				result.put("status", HttpStatus.INTERNAL_SERVER_ERROR.value());
				result.put("messages", e.getMessage());
			}
			
		}
		
		return result;
	}
	
	@Override
	public Map<String,Object> updateUser(User user, HttpHeaders headers) throws Exception {
		Map<String, Object> result = new HashMap<String, Object>();
		
		List<String> errorList = new ArrayList<String>();
		
		User userold = new User();
		
		Optional<User> userOpt = userRepository.findById(user.getId());
		userOpt.ifPresent(userx -> {
			
			BeanUtils.copyProperties(userx, userold);
			
		});
		
		errorList = getErrorListFromModel(userold, headers);
		
		if(errorList != null && errorList.size() > 0) {
			result.put("status", HttpStatus.BAD_REQUEST.value());
			result.put("messages", errorList);
		} else {
			userold.setFirstname(user.getFirstname());
			userold.setLastname(user.getLastname());
			userold.setDob(user.getDob());
			userold.setGender(user.getGender());
			
			userRepository.save(userold);
			result.put("status", HttpStatus.OK.value());
			result.put("messages", user);
		}
		
		return result;
	}
	
	@Override
	public List<User> getAllUser() throws Exception {
		return userRepository.findAll();
	}
	
	@Override
	public User getUserProfile(HttpHeaders headers) throws Exception {
		
		RestTemplate restTemplate = new RestTemplate();
		
		HttpEntity<String> entity = new HttpEntity<String>("parameters", headers);
	    
		User user = new User();
		
	    try {
	    	ResponseEntity<String> result = restTemplate.exchange(env.getProperty("user.profile.uri"), HttpMethod.GET, entity, String.class);

	    	user = userRepository.findByEmail(result.getBody());
	    	
	    } catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		
		return user;
		
	}
	
	@Override
	public Page<User> getPageableUser(Pagearea pagearea) throws Exception {
		
		List<Order> orders = new ArrayList<Order>();
		
		if(pagearea.getOrderBy() == null || pagearea.getOrderBy().size() < 1) {
			return (Page<User>) userRepository.findAll(PageRequest.of(pagearea.getPageNumber(), pagearea.getRowPerTable()));
		}
		
		String sort = StringUtils.isEmpty(pagearea.getSort()) ? Sort.Direction.ASC.toString() : pagearea.getSort().toUpperCase();
		
		pagearea.getOrderBy().forEach((string) -> {
			Order order1 = new Order(Sort.Direction.DESC.toString().equals(sort) ? Sort.Direction.DESC : Sort.Direction.ASC, string);
			orders.add(order1);
		});
		
		return (Page<User>) userRepository.findAll(PageRequest.of(pagearea.getPageNumber(), pagearea.getRowPerTable(), Sort.by(orders)));
	}
	
}
