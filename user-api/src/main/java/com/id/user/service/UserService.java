package com.id.user.service;

import java.util.List;
import java.util.Map;

import org.springframework.data.domain.Page;
import org.springframework.http.HttpHeaders;

import com.id.user.model.db.dataSourceOne.User;
import com.id.user.model.var.Pagearea;

public interface UserService {

	public Map<String,Object> registerUser(User user, HttpHeaders headers) throws Exception;
	public List<User> getAllUser() throws Exception;
	public Page<User> getPageableUser(Pagearea pagearea) throws Exception;
	public Map<String, Object> updateUser(User user, HttpHeaders headers) throws Exception;
	public User getUserProfile(HttpHeaders headers) throws Exception;
	
}
