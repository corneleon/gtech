package com.id.user.model.db.dataSourceOne;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.id.user.config.DateDeserializer;
import com.id.user.config.DateSerializer;

@Entity
@Table(schema = "user")
public class User {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	
	@Column(length = 16)
	@Pattern(regexp="(^[0-9]*$)", message = "validation.notValid.handphone")
	@NotNull(message = "validation.notNull.handphone")
	private String handphone;
	
	@JsonProperty(access = Access.WRITE_ONLY)
	@Column(length = 255)
	@NotNull(message = "validation.notNull.password")
	private String password;
	
	@Column(length = 255)
	@NotNull(message = "validation.notNull.firstname")
	private String firstname;
	
	@Column(length = 255)
	@NotNull(message = "validation.notNull.lastname")
	private String lastname;
	
	@Column(length = 255)
	@NotNull(message = "validation.notNull.email")
	@Email(message = "validation.notValid.email")
	private String email;
	
	@Column(length = 1)
	private String gender;
	
	@JsonDeserialize(using = DateDeserializer.class)
	@JsonSerialize(using = DateSerializer.class)
	private Date dob;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getHandphone() {
		return handphone;
	}

	public void setHandphone(String handphone) {
		this.handphone = handphone;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public Date getDob() {
		return dob;
	}

	public void setDob(Date dob) {
		this.dob = dob;
	}

	
}
