package com.id.user.model.var;

import java.util.List;

public class Pagearea {

	private int pageNumber = 1;
	
	private int rowPerTable = 10;
	
	private List<String> orderBy;
	
	private String sort;

	public int getPageNumber() {
		return pageNumber;
	}

	public void setPageNumber(int pageNumber) {
		this.pageNumber = pageNumber;
	}

	public int getRowPerTable() {
		return rowPerTable;
	}

	public void setRowPerTable(int rowPerTable) {
		this.rowPerTable = rowPerTable;
	}

	public List<String> getOrderBy() {
		return orderBy;
	}

	public void setOrderBy(List<String> orderBy) {
		this.orderBy = orderBy;
	}

	public String getSort() {
		return sort;
	}

	public void setSort(String sort) {
		this.sort = sort;
	}

}
