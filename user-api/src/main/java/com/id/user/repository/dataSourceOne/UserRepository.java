package com.id.user.repository.dataSourceOne;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.id.user.model.db.dataSourceOne.User;

@Repository
public interface UserRepository extends JpaRepository<User, java.lang.Long> {
	
	public User findByEmail(String email);

}
