package com.id.gateway.api;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.gateway.filter.GatewayFilter;
import org.springframework.cloud.gateway.filter.factory.AbstractGatewayFilterFactory;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.server.ServerWebExchange;

import com.auth0.jwt.exceptions.JWTVerificationException;

import reactor.core.publisher.Mono;


@Component
public class JWTFilter extends AbstractGatewayFilterFactory<JWTFilter.Config> {

	private static final String WWW_AUTH_HEADER = "WWW-Authenticate";
    private static final String X_JWT_SUB_HEADER = "X-jwt-sub";

    private static final Logger logger = LoggerFactory.getLogger(JWTFilter.class);
    
    @Autowired
    private Environment env;
    
    public JWTFilter() {
		super(Config.class);
	}

    public static class Config {
		// Put the configuration properties
	}

	@Override
	public GatewayFilter apply(Config config) {
		return (exchange, chain) -> {

            try {
                String token = this.extractJWTToken(exchange.getRequest());
                
        	    RestTemplate restTemplate = new RestTemplate();
        	    
        	    HttpHeaders headers = new HttpHeaders();
        	    headers.set("Authorization", token);
        	    
        	    HttpEntity<String> entity = new HttpEntity<String>("parameters", headers);
        	    
        	    try {
        	    	ResponseEntity<String> result = restTemplate.exchange(env.getProperty("auth.uri"), HttpMethod.GET, entity, String.class);
				} catch (Exception e) {
					return this.onError(exchange);
				}

        	    return chain.filter(exchange).then(Mono.fromRunnable(() -> {
    				System.out.println("First post filter");
    			}));

            } catch (JWTVerificationException ex) {

                logger.error(ex.toString());
                return this.onError(exchange);
            }
        };
	}
	
	private Mono<Void> onError(ServerWebExchange exchange)
    {
        ServerHttpResponse response = exchange.getResponse();
        response.setStatusCode(HttpStatus.UNAUTHORIZED);
        response.getHeaders().add(WWW_AUTH_HEADER, "Unauthorized");

        return response.setComplete();
    }
	
	private String extractJWTToken(ServerHttpRequest request)
    {
        if (!request.getHeaders().containsKey("Authorization")) {
            throw new JWTTokenExtractException("Authorization header is missing");
        }

        List<String> headers = request.getHeaders().get("Authorization");
        if (headers.isEmpty()) {
            throw new JWTTokenExtractException("Authorization header is empty");
        }

        String credential = headers.get(0).trim();
        String[] components = credential.split("\\s");

        if (components.length != 2) {
            throw new JWTTokenExtractException("Malformat Authorization content");
        }

        if (!components[0].equals("Bearer")) {
            throw new JWTTokenExtractException("Bearer is needed");
        }

        return credential;
    }
	
}
