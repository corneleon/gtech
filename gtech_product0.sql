CREATE DATABASE  IF NOT EXISTS `gtech` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `gtech`;
-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: 192.168.1.112    Database: gtech
-- ------------------------------------------------------
-- Server version	5.7.18-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `product`
--

DROP TABLE IF EXISTS `product`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `prdCd` varchar(45) DEFAULT NULL,
  `prdNm` varchar(255) DEFAULT NULL,
  `prdPrice` decimal(17,2) DEFAULT NULL,
  `storeNm` varchar(255) DEFAULT NULL,
  `brandNm` varchar(255) DEFAULT NULL,
  `catNm` varchar(255) DEFAULT NULL,
  `prdDesc` varchar(2000) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `productCode_UNIQUE` (`prdCd`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product`
--

LOCK TABLES `product` WRITE;
/*!40000 ALTER TABLE `product` DISABLE KEYS */;
INSERT INTO `product` VALUES (1,'XC-XT5','XTRADA 5',6100000.00,'Roda Link','Polygon','Trail','The Xtrada 5 is designed for riders who want to ride on a wide variety of terrain. The ALX super light 6061 hydro formed aluminum frame uses heat treated, hydro formed and butted tubing to give an optimal strength to weight ratio. The tapered head tube provides stable, front-end precision and the new Boost hub spacing on this frame allows for a stiffer wheel with more tyre clearance. The Boost system has also allowed for shorter chain stays, which makes the back of the bike a lot easier to flick around corners.The Xtrada 5 utilises Polygon’s new Wheel Fit Size System whereby each frame size is paired with the wheel size that best fits the rider and sets them up for optimal efficiency and power transfer. Frame S, M, L feature 27.5” wheels and frame M, L, XL come with 29” wheels. The Xtrada56 is a true cross country mountain bike with a geometry built for speed and stability coupled with tough, trail-ready components. It is perfect for hitting the local single track or an all-day mountain adventure.'),(2,'EN-SK9','SISKIU N9',39500000.00,'Roda Link','Polygon','Enduro','Siskiu N adalah seri sepeda enduro terbaru dari Polygon Bikes yang menghadirkan teknologi dan komponen pilihan yang dipadukan secara sempurna untuk bersepeda di medan yang menantang. Menghadirkan performa sepeda yang agresif, gesit dan cepat untuk setiap pesepeda yang ingin menantang kemampuan bersepeda di medan tanjakan dan turunan yang bervariasi. Dibangun dengan frame ALX yang kuat, sebuah perpaduan alumunium yang lebih ringan Dihadirkan dalam 2 ukuran roda untuk memberikan pengalaman bersepeda yang optimal sesuai dengan ukuran tubuh pengendara. Dengan geometri frame yang modern, Siskiu N juga dihadirkan dengan sistem 1x yang mudah dirawat serta sistem full suspensi yang dilengkapi dengan teknologi one piece linkage untuk optimasi kinerja suspensi yang tangguh. FAUX Bar Suspension System pada sepeda ini dirancang khusus untuk dapat memberikan kenyamanan, pengendalian, traksi yang maksimal, dan gaya dorong yang efisien saat melewati rintangan. Siskiu N, Exceed your Expectation!'),(3,'EN-SK8','SISKIU N8',32500000.00,'Roda Link','Polygon','Enduro','Siskiu N adalah seri sepeda enduro terbaru dari Polygon Bikes yang menghadirkan teknologi dan komponen pilihan yang dipadukan secara sempurna untuk bersepeda di medan yang menantang. Menghadirkan performa sepeda yang agresif, gesit dan cepat untuk setiap pesepeda yang ingin menantang kemampuan bersepeda di medan tanjakan dan turunan yang bervariasi. Dibangun dengan frame ALX yang kuat, sebuah perpaduan alumunium yang lebih ringan Dihadirkan dalam 2 ukuran roda untuk memberikan pengalaman bersepeda yang optimal sesuai dengan ukuran tubuh pengendara. Dengan geometri frame yang modern, Siskiu N juga dihadirkan dengan sistem 1x yang mudah dirawat serta sistem full suspensi yang dilengkapi dengan teknologi one piece linkage untuk optimasi kinerja suspensi yang tangguh. FAUX Bar Suspension System pada sepeda ini dirancang khusus untuk dapat memberikan kenyamanan, pengendalian, traksi yang maksimal, dan gaya dorong yang efisien saat melewati rintangan. Siskiu N, Exceed your Expectation!'),(4,'EN-SK7','SISKIU T7',21800000.00,'Roda Link','Polygon','Enduro','Riders’ dreams aren’t limited by the depth of their pockets. The latest evolution of the Siskiu T series opens up any trail to any rider through encompassing progressive geometry into an attainable package with a water bottle space! On the trail, the Siskiu T feels just as responsive and agile as the previous generation but the overall ceiling of what the bike is capable of has been lifted. The Siskiu T handles the demands of a modern trail bike, striking the balance between high-speed control and playful handling. Designed around our Wheel Fit Size System, 29” models feature 135mm of rear travel with 140mm forks whilst 27.5” bikes have 140mm in the rear with 150mm up front. Seat tubes have been significantly shortened to allow for 150mm dropper posts on the small and medium frame, 170mm on large and XL. The chainstays were shortened to keep the bike fun but also ensure that the overall wheelbase isn’t too long for general trail riding. The Siskiu T utilizes the tried and tested FAUX BAR design. The linkage is built around the One Piece Unibody Pivot Bridge that reduces the frame’s weight but also improves stiffness and reduces lateral flex on the rear shock under load. The kinematics of the bike maintains the poppy supportive nature of the previous generation while improving the climbing efficiency and small bump compliance.If you’re looking for a bike to do it all with killer style and a dependable, high quality, trail-ready components list – Siskiu T is the right choice for you. For any trail, any rider.'),(5,'SP-CLO','CLEO 2',3950000.00,'Roda Link','Polygon','Sport','Cleo 2 dirancang khusus sebagai sepeda komuter khusus wanita yang cocok untuk medan perkotaan hingga medan off road ringan. Didesain khusus dengan geometri sepeda gunung, Cleo 2 menggunakan frame alloy yang kuat dan ringan, roda berukuran 27.5″, posisi duduk yang lebih tegak dan nyaman. Cleo 2 diciptakan untuk wanita yang ingin berpetualang lebih jauh.'),(6,'SP-RYZ','RAYZ',3600000.00,'Roda Link','Polygon','Sport','Sepeda gunung serbaguna yang akan menemani mobilitas anda mulai dari kantor hingga bersepeda santai di weekend. Dirancang dengan frame alloy AL6 yang ringan dan kuat, serta geometri yang modern untuk kenyamanan optimum. Hadir dengan roda berukuran 27,5” untuk traksi lebih baik, dilengkapi dengan rearshock air suspension untuk meredam benturan lebih baik dan tubing seat tube yang lebih kokoh. Rayz one menjadi teman yang menyenangkan untuk bersepeda sehari-hari dan setiap aktivitas sport Anda.');
/*!40000 ALTER TABLE `product` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-08-26 15:36:51
