package com.id.product.model.db;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(schema = "product")
public class Product {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	
	@Column(name = "prdcd", length = 45)
	@NotNull(message = "validation.notNull.prdCd")
	private String productCode;
	
	@Column(name = "prdnm", length = 255)
	@NotNull(message = "validation.notNull.prdNm")
	private String productName;
	
	@Column(name = "prdprice")
	private BigDecimal productPrice = BigDecimal.ZERO;
	
	@Column(name = "storenm", length = 255)
	@NotNull(message = "validation.notNull.storeNm")
	private String storeName;
	
	@Column(name = "brandnm", length = 255)
	@NotNull(message = "validation.notNull.brandNm")
	private String brandName;
	
	@Column(name = "catnm", length = 255)
	@NotNull(message = "validation.notNull.catNm")
	private String categoryName;
	
	@Column(name = "prddesc", length = 2000)
	@NotNull(message = "validation.notNull.prdDesc")
	private String productDesc;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public BigDecimal getProductPrice() {
		return productPrice;
	}

	public void setProductPrice(BigDecimal productPrice) {
		this.productPrice = productPrice;
	}

	public String getStoreName() {
		return storeName;
	}

	public void setStoreName(String storeName) {
		this.storeName = storeName;
	}

	public String getBrandName() {
		return brandName;
	}

	public void setBrandName(String brandName) {
		this.brandName = brandName;
	}

	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	public String getProductDesc() {
		return productDesc;
	}

	public void setProductDesc(String productDesc) {
		this.productDesc = productDesc;
	}
	
}
