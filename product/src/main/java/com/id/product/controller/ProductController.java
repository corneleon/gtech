package com.id.product.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.id.product.model.db.Product;
import com.id.product.model.var.Pagearea;
import com.id.product.service.ProductService;

@RestController
public class ProductController {
	
	@Autowired
	private ProductService productService;

	@RequestMapping(value = "/getDetail", method = RequestMethod.GET)
	public ResponseEntity<?> getProfile(@RequestParam Long id) throws Exception {
		
		return ResponseEntity.status(HttpStatus.OK).body(productService.getDetail(id));
		
	}
	
	@RequestMapping(value = "/getAll", method = RequestMethod.GET)
	public ResponseEntity<?> getAll() throws Exception {
		
		return ResponseEntity.status(HttpStatus.OK).body(productService.getAll());
	}
	
	@RequestMapping(value = "/createProduct", method = RequestMethod.POST)
	public ResponseEntity<?> createProduct(@RequestBody Product product) throws Exception {
		
		return ResponseEntity.status(HttpStatus.OK).body(productService.createProduct(product));
	}
	
	//Just For Insert DB
	@RequestMapping(value = "/getPageable", method = RequestMethod.POST)
	public ResponseEntity<?> getPageable(@RequestBody Pagearea pagearea) throws Exception {
		
		return ResponseEntity.status(HttpStatus.OK).body(productService.getPageableProduct(pagearea));
	}
	
}
