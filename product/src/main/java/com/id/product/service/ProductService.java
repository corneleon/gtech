package com.id.product.service;

import java.util.List;

import org.springframework.data.domain.Page;

import com.id.product.model.db.Product;
import com.id.product.model.var.Pagearea;

public interface ProductService {

	public List<Product> getAll() throws Exception;

	public Page<Product> getPageableProduct(Pagearea pagearea) throws Exception;

	public Product getDetail(Long id) throws Exception;

	public Product createProduct(Product prod) throws Exception;

}
