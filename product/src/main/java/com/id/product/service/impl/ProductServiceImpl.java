package com.id.product.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Order;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.id.product.model.db.Product;
import com.id.product.model.var.Pagearea;
import com.id.product.repository.ProductRepository;
import com.id.product.service.ProductService;

@Service
public class ProductServiceImpl implements ProductService {
	
	@Autowired
	private ProductRepository productRepository;
	
	@Override
	public Product createProduct(Product prod) throws Exception{
		
		return productRepository.saveAndFlush(prod);
		
	}
	
	@Override
	public Product getDetail(Long id) throws Exception{
		
		Product product = new Product();
		
		Optional<Product> optProd = productRepository.findById(id);
		optProd.ifPresent(prod -> {
			BeanUtils.copyProperties(prod, product);
		});
		
		return product;
		
	}
	
	@Override
	public List<Product> getAll() throws Exception{
		
		return productRepository.findAll();
		
	}
	
	@Override
	public Page<Product> getPageableProduct(Pagearea pagearea) throws Exception {
		
		List<Order> orders = new ArrayList<Order>();
		
		if(pagearea.getOrderBy() == null || pagearea.getOrderBy().size() < 1) {
			return (Page<Product>) productRepository.findAll(PageRequest.of(pagearea.getPageNumber(), pagearea.getRowPerTable()));
		}
		
		String sort = StringUtils.isEmpty(pagearea.getSort()) ? Sort.Direction.ASC.toString() : pagearea.getSort().toUpperCase();
		
		pagearea.getOrderBy().forEach((string) -> {
			Order order1 = new Order(Sort.Direction.DESC.toString().equals(sort) ? Sort.Direction.DESC : Sort.Direction.ASC, string);
			orders.add(order1);
		});
		
		return (Page<Product>) productRepository.findAll(PageRequest.of(pagearea.getPageNumber(), pagearea.getRowPerTable(), Sort.by(orders)));
	}

}
